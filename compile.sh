#!/bin/bash

# cleanup
echo "cleaning up..."
rm -rf compiled/

# make compile directory
echo "creating directory for compiled files..."
mkdir compiled

# compile project with MAVEN
mvn clean install

# copy resources
echo "copying resources to compiled folder..."
cp -R resources/win-bin compiled/
cp -R resources/example compiled/
cp resources/start-linux.sh compiled/
cp resources/start-windows.bat compiled/
cp LICENSE compiled/
cp README compiled/

echo "copying program to compiled folder..."
mv target/ArduinoFirmwareUploader* compiled/ArduinoFirmwareUploader.jar

echo "done."
