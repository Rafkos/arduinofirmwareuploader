package net.rafkos.tools.ArduinoFirmwareUploader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import com.fazecast.jSerialComm.SerialPort;
import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;

public class Uploader extends Thread
{
	private static Semaphore sem = new Semaphore(1);
	private App app = null;
	private File hexFile = null;
	private File avrconfFile = null;
	private File tempDir = null;
	private ArrayList<String> avrdudeArgs = new ArrayList<String>();
	private SerialPort port = null;
	private File romFile = null;
	private String avrdude = null;
	private boolean success;
	private String result = null;

	public Uploader(App app)
	{
		this.app = app;
	}

	@Override
	public void run()
	{
		// ensure atomic upload
		try
		{
			sem.acquire();
		}catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		// freeze user's GUI
		app.lockProgram();
		if(init())
		{
			upload();
		}
		cleanup();
		// report
		app.message(result, success);
		app.unlockProgram();
		sem.release();
	}

	private boolean init()
	{
		// null values from previous session
		hexFile = null;
		avrconfFile = null;
		tempDir = null;
		avrdudeArgs = new ArrayList<String>();
		romFile = null;
		port = null;
		avrdude = null;
		result = null;
		success = false;

		app.clearlog();

		if(!App.getSystem().equalsIgnoreCase("windows") && !App.getSystem().equalsIgnoreCase("linux"))
		{
			reportError("Error. Your OS is not supported.");
			return false;
		}

		locateAvrdude();

		// avrdude wasn't located
		if(avrdude == null)
		{
			reportError("Error. Could not find avrdude installation on this computer.");
			return false;
		}

		// check if user provided correct data
		port = app.getSerialPort();
		romFile = app.getRomFile();
		if(port == null || romFile == null)
		{
			reportError("Error. No serial port or ROM file provided.");
			return false;
		}

		return true;
	}

	private void upload()
	{

		// check if unpacking was completed
		if(unpackRomFile())
		{
			if(collectData())
			{
				if(executeAvrdude())
				{
					reportSuccess("Firmware updated correctly.");
				}else
				{
					if(App.getSystem().equalsIgnoreCase("windows"))
					{
						reportError("An error has occured during the upload of the ROM file.\n"
								+ "Incorrect port or ROM file configuration. \n"
								+ "Make sure WinAVR and the device's drivers are installed.");
					}else
					{
						reportError("An error has occured during the upload of the ROM file.\n"
								+ "Incorrect port or ROM file configuration.");
					}
				}
			}else
			{
				reportError("Error. ROM file is incorrect or corrupted.");
			}
		}else
		{
			reportError("An error has occured during unpacking provided ROM file.");
		}

	}

	private boolean unpackRomFile()
	{
		if(romFile.exists() && romFile.isFile())
		{
			if(App.getSystem().equalsIgnoreCase("windows"))
			{
				tempDir = new File(System.getProperty("java.io.tmpdir") + "\\_tmp_afu_" + System.currentTimeMillis());
			}else if(App.getSystem().equalsIgnoreCase("linux"))
			{
				tempDir = new File("_tmp_afu_" + System.currentTimeMillis());
			}else
			{
				return false;
			}

			if(tempDir.mkdir())
			{
				String zipFilePath = romFile.getPath();
				String destDir = tempDir.getPath();

				// unzip
				unzip(zipFilePath, destDir);

				return true;
			}
		}

		return false;
	}

	private boolean collectData()
	{
		hexFile = new File(tempDir.getPath() + "/rom.hex");
		avrconfFile = new File(tempDir.getPath() + "/avrdude.conf");

		Scanner in = null;

		try
		{
			in = new Scanner(new File(tempDir.getPath() + "/args.dat"));
			avrdudeArgs.add(avrdude);
			while(in.hasNext())
			{
				String line = in.nextLine();
				line = parseAvrdudeArg(line);
				avrdudeArgs.add(line);
			}
		}catch(FileNotFoundException e)
		{
			return false;
		}finally
		{
			if(in != null)
			{
				in.close();
			}
		}

		if(!hexFile.exists() || !avrconfFile.exists() || avrdudeArgs.isEmpty())
		{
			return false;
		}

		return true;
	}

	private void locateAvrdude()
	{
		if(App.getSystem().equalsIgnoreCase("windows"))
		{
			// for Windows there should be a binary file provided with this program
			URL jarLocationUrl = App.class.getProtectionDomain().getCodeSource().getLocation();
			String jarLocation = new File(jarLocationUrl.toString()).getParent();
			try
			{
				jarLocation = URLDecoder.decode(jarLocation, "UTF-8");
			}catch(UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
			String binaryFileLocation = (jarLocation + "\\win-bin\\avrdude.exe").substring(6);
			if((new File(binaryFileLocation)).exists())
			{
				avrdude = binaryFileLocation;
			}

		}else if(App.getSystem().equalsIgnoreCase("linux"))
		{
			// for Linux avrdude is expected to be installed by default
			if((new File("/bin/avrdude")).exists())
			{
				avrdude = "avrdude";
			}
		}
	}

	private String parseAvrdudeArg(String line)
	{
		line = line.replace("<CONF>", avrconfFile.getAbsolutePath());
		line = line.replace("<HEX>", hexFile.getAbsolutePath());

		if(App.getSystem().equalsIgnoreCase("linux"))
		{
			line = line.replace("<PORT>", "/dev/" + port.getSystemPortName());
		}else if(App.getSystem().equalsIgnoreCase("windows"))
		{
			line = line.replace("<PORT>", port.getSystemPortName());
		}
		return line;
	}

	private boolean executeAvrdude()
	{
		ProcessBuilder processBuilder = new ProcessBuilder();
		String[] args = avrdudeArgs.toArray(new String[avrdudeArgs.size()]);
		processBuilder.command(args);

		try
		{
			Process process = processBuilder.start();

			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream(), "UTF-8"));

			boolean flag = false;
			TimeLimiter timeLimiter = new SimpleTimeLimiter();

			String line = "";

			while(line != null)
			{
				if(line.contains("bytes of flash verified"))
				{
					flag = true;
				}
				app.log(line + "\n");
				try
				{
					line = timeLimiter.callWithTimeout(reader::readLine, 5L, TimeUnit.SECONDS, true);
				}catch(Exception e)
				{
					// timeout
					process.destroy();
					break;
				}
			}

			int exitVal = process.waitFor();
			if(exitVal == 0)
			{
				return flag;
			}else
			{
				return false;
			}

		}catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}catch(InterruptedException e)
		{
			e.printStackTrace();
			return false;
		}

	}

	private void cleanup()
	{
		// remove temp files
		if(tempDir != null)
		{
			if(tempDir.isDirectory())
			{
				try
				{
					deleteDirectoryStream(tempDir.toPath());
				}catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private void deleteDirectoryStream(Path path) throws IOException
	{
		Files.walk(path).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
	}

	private void reportError(String err)
	{
		success = false;
		result = err;
	}

	private void reportSuccess(String msg)
	{
		success = true;
		result = msg;
	}

	/*
	 * Courtesy of: https://www.journaldev.com/960/java-unzip-file-example
	 */
	private void unzip(String zipFilePath, String destDir)
	{
		File dir = new File(destDir);

		// create output directory if it doesn't exist
		if(!dir.exists())
			dir.mkdirs();
		FileInputStream fis;

		// buffer for read and write data to file
		byte[] buffer = new byte[1024];
		try
		{
			fis = new FileInputStream(zipFilePath);
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = zis.getNextEntry();
			while(ze != null)
			{
				String fileName = ze.getName();
				File newFile = new File(destDir + File.separator + fileName);
				app.log("Unzipping ROM to " + newFile.getAbsolutePath() + "\n");

				// create directories for sub directories in zip
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while((len = zis.read(buffer)) > 0)
				{
					fos.write(buffer, 0, len);
				}
				fos.close();
				// close this ZipEntry
				zis.closeEntry();
				ze = zis.getNextEntry();
			}
			// close last ZipEntry
			zis.closeEntry();
			zis.close();
			fis.close();
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
