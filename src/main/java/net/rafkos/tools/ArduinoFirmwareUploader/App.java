package net.rafkos.tools.ArduinoFirmwareUploader;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import com.fazecast.jSerialComm.SerialPort;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class App
{
	private JFrame mainFrame;
	private JTextField romPathTextField;
	private JComboBox<SerialPortContainer> commPortComboBox;
	private CommUpdateThread commUpdateThread;
	private JTextArea logTextArea;
	private JButton btnUpload;

	@Getter
	private static String system;

	@Getter
	private File romFile = null;

	@Getter
	private SerialPort serialPort = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1)
		{
			e1.printStackTrace();
		}
		if(isSystemSupported())
		{
			EventQueue.invokeLater(new Runnable()
			{
				public void run()
				{
					try
					{
						App window = new App();
						window.mainFrame.setVisible(true);
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			});
		}
	}

	private static boolean isSystemSupported()
	{
		String s = System.getProperty("os.name");
		if(s.contains("Windows"))
		{
			system = "windows";
			return true;
		}else if(s.contains("Linux"))
		{
			system = "linux";
			return true;
		}
		return false;
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public App()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		mainFrame = new JFrame();
		mainFrame.setTitle("Arduino ROM Uploader");
		mainFrame.setBounds(100, 100, 550, 447);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.getContentPane().setLayout(new BoxLayout(mainFrame.getContentPane(), BoxLayout.X_AXIS));

		JSplitPane mainSplit = new JSplitPane();
		mainSplit.setEnabled(false);
		mainSplit.setOrientation(JSplitPane.VERTICAL_SPLIT);
		mainSplit.setDividerSize(0);
		mainFrame.getContentPane().add(mainSplit);

		JPanel mainSplitTopPanel = new JPanel();
		mainSplitTopPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		mainSplit.setLeftComponent(mainSplitTopPanel);
		mainSplitTopPanel.setLayout(new GridLayout(2, 2, 4, 4));
		JLabel lblCommPort = new JLabel("Comm port:");
		mainSplitTopPanel.add(lblCommPort);

		JLabel lblRomFile = new JLabel("Rom file:");
		mainSplitTopPanel.add(lblRomFile);

		romPathTextField = new JTextField();

		JLabel emptyFiller = new JLabel("");
		mainSplitTopPanel.add(emptyFiller);

		commPortComboBox = new JComboBox<SerialPortContainer>();
		mainSplitTopPanel.add(commPortComboBox);
		commPortComboBox.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				SerialPortContainer container = (SerialPortContainer) commPortComboBox.getSelectedItem();
				if(container != null)
				{
					serialPort = container.getSerialPort();
				}
			}
		});
		mainSplitTopPanel.setFocusTraversalPolicy(new FocusTraversalOnArray(
				new Component[] { lblRomFile, commPortComboBox, romPathTextField, lblCommPort }));
		mainSplitTopPanel.add(romPathTextField);
		romPathTextField.setEditable(false);
		romPathTextField.setColumns(10);

		JButton btnBrowse = new JButton("browse");
		btnBrowse.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				FileNameExtensionFilter filter = new FileNameExtensionFilter("ROM files", ".afu", "afu");
				jfc.setFileFilter(filter);

				int returnValue = jfc.showOpenDialog(null);

				if(returnValue == JFileChooser.APPROVE_OPTION)
				{
					File selectedFile = jfc.getSelectedFile();
					romFile = selectedFile;
					romPathTextField.setText(selectedFile.getAbsolutePath());
				}
			}
		});

		mainSplitTopPanel.add(btnBrowse);

		JPanel mainSplitBottomPanel = new JPanel();
		mainSplit.setRightComponent(mainSplitBottomPanel);
		mainSplitBottomPanel.setLayout(new BoxLayout(mainSplitBottomPanel, BoxLayout.X_AXIS));

		JSplitPane innerSplit = new JSplitPane();
		innerSplit.setEnabled(false);
		innerSplit.setResizeWeight(1.0);
		innerSplit.setOrientation(JSplitPane.VERTICAL_SPLIT);
		innerSplit.setDividerSize(0);
		mainSplitBottomPanel.add(innerSplit);

		JPanel innerSplitBottomPanel = new JPanel();
		innerSplit.setRightComponent(innerSplitBottomPanel);
		innerSplitBottomPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

		btnUpload = new JButton("Upload");
		innerSplitBottomPanel.add(btnUpload);
		btnUpload.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				initUpload();
			}
		});

		JPanel innerSplitTopPanel = new JPanel();
		innerSplit.setLeftComponent(innerSplitTopPanel);
		innerSplitTopPanel.setLayout(new BoxLayout(innerSplitTopPanel, BoxLayout.X_AXIS));

		JScrollPane logScrollPane = new JScrollPane();
		innerSplitTopPanel.add(logScrollPane);

		logTextArea = new JTextArea();
		logTextArea.setEditable(false);
		logTextArea.setText("Hello");
		logScrollPane.setViewportView(logTextArea);

		commUpdateThread = new CommUpdateThread();
		commUpdateThread.start();

		mainFrame.setVisible(true);
	}

	protected void initUpload()
	{
		Uploader uploader = new Uploader(this);
		uploader.start();
	}

	protected void updateCommPorts()
	{
		commPortComboBox.removeAllItems();
		for(SerialPort port : SerialPort.getCommPorts())
		{
			commPortComboBox.addItem(new SerialPortContainer(port));
		}
	}

	public void clearlog()
	{
		logTextArea.setText("");
	}

	public void log(String str)
	{
		logTextArea.append(str);
		logTextArea.setCaretPosition(logTextArea.getText().length());
	}

	public void message(String msg, boolean success)
	{
		int type = JOptionPane.ERROR_MESSAGE;
		if(success)
		{
			type = JOptionPane.INFORMATION_MESSAGE;
		}

		JOptionPane.showMessageDialog(mainFrame, msg, "Upload status", type);
	}

	public void lockProgram()
	{
		mainFrame.setEnabled(false);
		btnUpload.setEnabled(false);
	}

	public void unlockProgram()
	{
		mainFrame.setEnabled(true);
		btnUpload.setEnabled(true);
	}

	private class CommUpdateThread extends Thread
	{
		private boolean enabled = true;
		private String[] comparators = new String[1];

		@Override
		public void run()
		{
			while(enabled)
			{
				SerialPort[] currentCommPorts = SerialPort.getCommPorts();
				if(portsChanged(currentCommPorts))
				{
					updateComparators(currentCommPorts);
					updateCommPorts();
				}
				try
				{
					sleep(200);
				}catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}

		private void updateComparators(SerialPort[] currentCommPorts)
		{
			comparators = new String[currentCommPorts.length];
			for(int i = 0; i < comparators.length; i++)
			{
				comparators[i] = currentCommPorts[i].getDescriptivePortName();
			}

		}

		private boolean portsChanged(SerialPort[] currentCommPorts)
		{
			if(currentCommPorts.length == comparators.length)
			{
				for(int i = 0; i < comparators.length; i++)
				{
					SerialPort port = currentCommPorts[i];
					if(comparators[i] != null)
					{
						if(comparators[i].equals(port.getDescriptivePortName()))
						{
							return false;
						}
					}
				}
			}

			return true;
		}
	}

	@AllArgsConstructor
	@Getter
	private class SerialPortContainer
	{
		private SerialPort serialPort;

		@Override
		public String toString()
		{
			return serialPort.getSystemPortName();
		}

	}
}
