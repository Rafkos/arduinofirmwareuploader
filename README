# ABOUT

This software's purpose is to upload a specially crafted ROM file 
to given Arduino board. ROM file is a zip file with .afu extension
which contains Avrdude's configuration, a program in binary form 
and a list of arguments for Avrdude. It can be used by board's 
manufacturers for providing an easy way to upgrade software of
their products without providing any source code.

Avrdude is a piece of software made for interacting with Arduino
boards and other microcontrollers. It's being used by
ArduinoFirmwareUploader, windows binary is included in the software's
win-bin folder. 

author: Rafał Kosyl
admin@rafkos.net

Licensed under GPLv3.

# ROM file

ROM file is an archive that contains data provided by the manufacturer.
Please note that the ROM will work only with a specific board (Arduino),
an information about compatibility should be provided by the manufacturer.
Rom file consists of 3 files:
rom.hex - a compiled source of Arduino program.
avrdude.conf - a configuration file of Avrdude.
args.dat - a list of arguments (one per line)
Please refer to the example/ folder for an example ROM file.

# INSTALLATION

Windows
- make sure WinAVR is installed, it is required for COMM port communication.
- make sure Java 8 or newer is installed on your computer.
- install any other drivers provided by the board's manufacturer.

Linux
- make sure avrdude package is installed on your distro.
- install Java version 8 or newer.
- install any other drivers provided by the board's manufacturer.

# USAGE

Windows
- start ArduinoFirmwareUploader with start-windows.bat.
- connect your device and select its port.
- choose ROM file and click "Upload" button.
- wait for results.

Linux
- start ArduinoFirmwareUploader with ./start-linux.sh.
- connect your device and select its port.
- choose ROM file and click "Upload" button.
- wait for results.

# COMPILE

For newest source visit:
https://bitbucket.org/Rafkos/arduinofirmwareuploader/src/master/

To use an automatic compilation script you will need to run 64bit Linux.
Make sure maven and java8 packages are installed. Run ./compile.sh and wait.
Compiled source will be available in compiled/ folder.

